group = "org.digieng"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.3.72"
}

repositories {
    mavenCentral()
    jcenter()
}

kotlin {
    linuxX64("linux") {
        compilations.getByName("main") {
            cinterops.create("ncurses") {
                val ncursesDir = "${System.getProperty("user.home")}/ncurses-6.1"
                val includeDir = "$ncursesDir/include"
                includeDirs(includeDir)
                compilerOpts(includeDir)
                extraOpts("-libraryPath", "$ncursesDir/lib")
            }
        }
        binaries {
            executable("jmonitor") {
                entryPoint = "org.digieng.jmonitor.main"
            }
        }
    }
}