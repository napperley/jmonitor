@file:Suppress("EXPERIMENTAL_UNSIGNED_LITERALS", "EXPERIMENTAL_API_USAGE")

package org.digieng.jmonitor

import kotlinx.cinterop.*
import ncurses.*
import platform.posix.*
import kotlin.system.exitProcess

private val jpsCommand = "${getenv("JAVA_HOME")?.toKString()}/bin/jps"

fun main(args: Array<String>) {
    if (args.size == 1 && "-l" in args) listJvmProcesses()
    else if (args.size == 2 && args.first() == "-s") terminateJvmProcess(args[1], false)
    else if (args.size == 2 && args.first() == "-k") terminateJvmProcess(args[1], true)
    else if (args.size == 2 && args.first() == "--io-metrics") printIOMetrics(args[1])
    else if (args.size == 2 && args.first() == "--net-metrics") printNetworkMetrics(args[1])
    else if (args.size == 2 && args.first() == "--realtime-io-metrics") displayRealtimeIoMetrics(args[1])
    else printUsage()
}

private fun initNcurses() {
    // Initialise ncurses.
    initscr()
    // Turn off displaying keyboard input.
    noecho()
    // Turn on colour support.
    start_color()
    createColours()
}

private fun createColours() {
    // Create colour pairs.
    init_pair(1, COLOR_GREEN.toShort(), COLOR_BLACK.toShort())
    init_pair(2, COLOR_BLUE.toShort(), COLOR_BLACK.toShort())
    init_pair(3, COLOR_RED.toShort(), COLOR_BLACK.toShort())
}

private fun displayRealtimeIoMetrics(process: String) {
    if (!processExists(process)) {
        println("JVM process $process doesn't exist.")
        exitProcess(-1)
    }
    initNcurses()
    attron(COLOR_PAIR(3))
    printw("Screen Size: ${screenWidth()}x${screenHeight()}")
    attroff(COLOR_PAIR(3))
    val win = createRealtimeIoMetricsWindow(process)

    moveCursor(21, 3)
    var char = -1
    while (char != 'q'.toInt()) {
        updateRealtimeIoMetricsWindow(process, win)
        char = getch()
        sleep(1u)
    }
    // Clean up the terminal.
    endwin()
}

private fun updateRealtimeIoMetricsWindow(process: String, win: CPointer<WINDOW>?) {
    var pos = 2
    fetchIoMetrics(process).forEach { m ->
        pos++
        printInWindow(win = win, msg = "* $m", row = pos, col = 3)
    }
    wrefresh(win)
}

private fun createRealtimeIoMetricsWindow(process: String): CPointer<WINDOW>? {
    val win = createWindow(width = 70, height = 20, startRow = 2, startCol = 4)
    refresh()
    applyWindowBorder(win = win)
    wrefresh(win)
    useColourPair(win, 1) {
        printInWindow(win = this, msg = " $process I/O Metrics ", row = 0, col = 3)
    }
    useColourPair(win, 2) {
        printInWindow(win = this, msg = "Press 'q' to quit.", row = 1, col = 3)
    }
    return win
}

private fun processExists(process: String): Boolean {
    val output = processJpsOutput(runCommand(jpsCommand))
    return output.any { it.first() == process || it.last() == process }
}

private fun fetchIoMetrics(process: String) = memScoped {
    val bufferSize = 150
    val buffer = allocArray<ByteVar>(bufferSize)
    val pid = if (process.containsInt()) process.toInt() else extractPid(process)
    val file = fopen("/proc/$pid/io", READ_MODE)
    val tmp = mutableListOf<String>()
    while (fgets(__stream = file, __n = bufferSize, __s = buffer) != null) {
        tmp += processIOMetricString(buffer.toKString())
    }
    fclose(file)
    tmp.toTypedArray()
}

private fun printIOMetrics(process: String) {
    if (processExists(process)) {
        println("I/O metrics for ${if (process.containsInt()) "PID " else ""}$process:")
        fetchIoMetrics(process).forEach { println("* $it") }
    } else {
        println("JVM process $process doesn't exist.")
        exit(-1)
    }
}

private fun printNetworkMetrics(process: String) = memScoped {
    val bufferSize = 150
    val buffer = allocArray<ByteVar>(bufferSize)
    if (processExists(process)) {
        val pid = if (process.containsInt()) process.toInt() else extractPid(process)
        val file = fopen("/proc/$pid/net/dev", READ_MODE)
        println("Network metrics for ${if (process.containsInt()) "PID " else ""}$process...")
        var pos = 1
        while (fgets(__stream = file, __n = bufferSize, __s = buffer) != null) {
            if (pos > 2) println(processNetworkMetricString(buffer.toKString()))
            pos++
        }
        fclose(file)
    } else {
        println("JVM process $process doesn't exist.")
        exit(-1)
    }
}

private fun processNetworkMetricString(str: String): String {
    val interfaceNamePos = 0
    val rxBytesPos = 1
    val rxPacketsPos = 2
    val rxErrorsPos = 3
    val rxDroppedPos = 4
    val txBytesPos = 9
    val txPacketsPos = 10
    val txErrorsPos = 11
    val txDroppedPos = 12
    var result = ""
    val tmp = str.split(" ").filter { it.isNotEmpty() }

    tmp.forEachIndexed { pos, s ->
        when (pos) {
            interfaceNamePos -> result += "-- ${s.replace(":", "")} Interface --\n"
            rxBytesPos -> result += "* RX Bytes: $s\n"
            rxPacketsPos -> result += "* RX Packets: $s\n"
            rxErrorsPos -> result += "* RX Errors: $s\n"
            rxDroppedPos -> result += "* RX Dropped: $s\n"
            txBytesPos -> result += "* TX Bytes: $s\n"
            txPacketsPos -> result += "* TX Packets: $s\n"
            txErrorsPos -> result += "* TX Errors: $s\n"
            txDroppedPos -> result += "* TX Dropped: $s\n"
        }
    }
    return result
}

private fun processIOMetricString(str: String): String {
    val tmp = str.removeSuffix("\n")
    val charReadStr = "Characters Read"
    val charWrittenStr = "Characters Written"
    val writtenCancelledStr = "Total Cancelled Written Bytes"
    val readSysCallsStr = "Total Read System Calls"
    val writeSysCallsStr = "Total Write System Calls"
    val bytesReadStr = "Total Bytes Read"
    val bytesWrittenStr = "Total Bytes Written"

    return when {
        "rchar" in tmp -> tmp.replace("rchar", charReadStr)
        "wchar" in tmp -> tmp.replace("wchar", charWrittenStr)
        "syscr" in tmp -> tmp.replace("syscr", readSysCallsStr)
        "syscw" in tmp -> tmp.replace("syscw", writeSysCallsStr)
        "read_bytes" in tmp -> tmp.replace("read_bytes", bytesReadStr)
        "cancelled_write_bytes" in tmp -> tmp.replace("cancelled_write_bytes", writtenCancelledStr)
        "write_bytes" in tmp -> tmp.replace("write_bytes", bytesWrittenStr)
        else -> tmp
    }
}

private fun terminateJvmProcess(process: String, killProcess: Boolean) {
    val signal = if (killProcess) SIGKILL else SIGTERM
    if (processExists(process)) {
        println("${if (killProcess) "Killing" else "Stopping"} JVM process $process...")
        if (process.containsInt()) kill(process.toInt(), signal)
        else kill(extractPid(process), signal)
    } else {
        println("JVM process $process doesn't exist.")
    }
}

private fun extractPid(processName: String) =
    processJpsOutput(runCommand(jpsCommand)).first { it.last() == processName }.first().toInt()

private fun printUsage() {
    println(
        """
            -- JMonitor Usage --
            * List JVM processes: jmonitor -l
            * Stop JVM process, eg: jmonitor -s GradleDaemon
            * Kill JVM process, eg: jmonitor -k GradleDaemon
            * Print I/O metrics, eg: jmonitor --io-metrics GradleDaemon
            * Print network metrics, eg: jmonitor --net-metrics GradleDaemon
            * Show I/O metrics in realtime, eg: jmonitor --realtime-io-metrics GradleDaemon 
        """.trimIndent()
    )
}

private fun processJpsOutput(output: String) = output
    .removeSuffix("\n")
    .split("\n")
    .filter { "Jps" !in it }
    .map { it.split(" ") }

private fun listJvmProcesses() {
    val output = processJpsOutput(runCommand(jpsCommand))
    println("JVM Processes:")
    output.forEach {
        val name = if (it.last().isNotEmpty()) it.last() else "??"
        println("* Name: $name, PID: ${it.first()}")
    }
}
