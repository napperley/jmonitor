@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.digieng.jmonitor

import kotlinx.cinterop.CPointer
import ncurses.*

internal fun createWindow(width: Int, height: Int, startRow: Int, startCol: Int) =
    newwin(height, width, startRow, startCol)

internal fun applyWindowBorder(win: CPointer<WINDOW>?, leftRightChar: Char = ' ', topBottomChar: Char = ' '): Boolean {
    val char1 = if (leftRightChar == ' ') 0u else leftRightChar.toInt().toUInt()
    val char2 = if (topBottomChar == ' ') 0u else topBottomChar.toInt().toUInt()
    return box(win, char1, char2) == 0
}

internal fun moveCursor(row: Int, col: Int) = move(row, col) == 0

internal fun printInWindow(win: CPointer<WINDOW>?, row: Int, col: Int, msg: String) =
    mvwprintw(win, row, col, msg) == 0

internal fun screenWidth() = getmaxx(stdscr)

internal fun screenHeight() = getmaxy(stdscr)

internal fun useColourPair(win: CPointer<WINDOW>?, colourPairId: Int, block: CPointer<WINDOW>?.() -> Unit) {
    wattron(win, COLOR_PAIR(colourPairId))
    block(win)
    wattroff(win, COLOR_PAIR(colourPairId))
}
