package org.digieng.jmonitor

import kotlinx.cinterop.ByteVar
import kotlinx.cinterop.allocArray
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.toKString
import platform.posix.fclose
import platform.posix.fgets
import platform.posix.popen

internal fun runCommand(command: String) = memScoped {
    var result = ""
    val readMode = "r"
    val bufferSize = 150
    val buffer = allocArray<ByteVar>(bufferSize)
    val processFile = popen(command, readMode)
    while (fgets(__stream = processFile, __n = bufferSize, __s = buffer) != null) result += buffer.toKString()
    fclose(processFile)
    result
}

fun String.containsInt() = try {
    toInt()
    true
} catch (ex: NumberFormatException) {
    false
}

internal const val READ_MODE = "r"