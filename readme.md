# JMonitor (jmonitor)

A CLI program that monitors and manages JVM processes. Can terminate processes and print out process metrics.


## Usage

To get help on program usage run `jmonitor`. List all running processes by running `jmonitor -l`. Stop a process (will allow a process to clean up before exiting) by running `jmonitor -s processName`. Kill a process (forces a process to exit immediately) by running `jmonitor -k processName`.

IO metrics can be printed by running `jmonitor --io-metrics processName`. Network metrics can be printed by running `jmonitor --net-metrics processName`.
